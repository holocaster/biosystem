package br.com.biosystem.loginmodule.security;

import java.security.Principal;
import java.security.acl.Group;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.jboss.security.SimpleGroup;

import br.com.biosystem.loginmodule.entities.Funcao;
import br.com.biosystem.loginmodule.entities.User;

public class BiosystemLoginModule implements LoginModule {

	protected static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmss");

	// Parametros
	protected static final String END_POINT = "EndPoint";
	protected static final String DEBUG = "debug";

	private String endPoint;
	private boolean debug = false;

	private boolean succeeded = false;
	private boolean commitSucceeded = false;
	private String username;
	private String ip;
	private char[] password;
	private User user;
	private List<Funcao> funcaoList;

	private Subject subject;
	@SuppressWarnings("rawtypes")
	private Map sharedState;
	private CallbackHandler callbackHandler;

	@Override
	public void initialize(final Subject subject, final CallbackHandler callbackHandler,
			final Map<String, ?> sharedState, final Map<String, ?> options) {
		this.subject = subject;
		this.sharedState = sharedState;
		this.callbackHandler = callbackHandler;
		this.endPoint = (String) options.get(END_POINT);
		this.debug = "true".equalsIgnoreCase(options.get(DEBUG) == null ? "false" : (String) options.get(DEBUG));
		if (this.debug) {
			System.out.println("\t\t[BiosystemLoginModule] " + END_POINT + " " + endPoint);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean login() throws LoginException {
		this.succeeded = false;
		this.loadUsernamePassword();
		if (this.username == null || this.username.trim().isEmpty()) {
			if (this.debug) {
				System.out.println("\t\t[BiosystemLoginModule] Usuario nao informado!");
			}
			throw new LoginException("Usuario nao informado!");
		}
		if (this.password == null || this.password.length == 0) {
			if (this.debug) {
				System.out.println("\t\t[BiosystemLoginModule] Senha nao informada!");
			}
			throw new LoginException("Senha nao informada!");
		}

		if ("admin".equals(this.username) && "admin".equals(new String(this.password))) {
			this.funcaoList = new ArrayList<>();
			this.funcaoList.add(new Funcao("acessar" , "tudo"));
			this.user = new User("admin" , "admin" );
			this.sharedState.put("javax.security.auth.principal", this.user);
			this.sharedState.put("javax.security.auth.roles", this.funcaoList);
			this.succeeded = true;
			return this.succeeded;
		} else {
			this.cleanState();
			this.succeeded = false;
			throw new LoginException("Usuario n�o identificado");
		}
		/*
		 * this.funcaoList = new ArrayList<Funcao>(); String noFuncionalidade = null;
		 * for (final FuncionalidadeHelper funcionalidade :
		 * matrizAcesso.getFuncionalidadeList()) { if (noFuncionalidade == null ||
		 * !noFuncionalidade.equals(funcionalidade.getNoFuncionalidade())) {
		 * this.funcaoList.add(new Funcao(funcionalidade.getNoFuncionalidade()));
		 * noFuncionalidade = funcionalidade.getNoFuncionalidade(); }
		 * this.funcaoList.add(new Funcao(funcionalidade.getNoFuncionalidade(),
		 * funcionalidade.getNoAcao(), funcionalidade.getCoTipoAbrangencia(),
		 * funcionalidade.getCoAbrangencia(), funcionalidade.getNuProcesso())); } final
		 * List<CaixaPerfil> perfis = new ArrayList<CaixaPerfil>(); for (final
		 * PerfilHelper perfilHelper : matrizAcesso.getPerfilList()) { perfis.add(new
		 * CaixaPerfil(perfilHelper.getNoPerfil(), perfilHelper.getDePerfil(),
		 * perfilHelper.getIcAutomatico(), perfilHelper.getIcLotacao(),
		 * perfilHelper.getIxPrioridade())); } Date dhUltimoAcesso = null; if
		 * (matrizAcesso.getDhUltimoAcesso() != null) { try { dhUltimoAcesso =
		 * SDF.parse(matrizAcesso.getDhUltimoAcesso()); } catch (final ParseException
		 * pe) { pe.printStackTrace(); } } final UsuarioHelper usuario =
		 * matrizAcesso.getUsuario(); this.user = new User(usuario.getDeMatricula(),
		 * usuario.getDeCpf(), usuario.getNoUsuario(), usuario.getCoUnidade(),
		 * usuario.getCoUnidadeAdm(), usuario.getSgUf(), usuario.getCoMuncipioIbge() !=
		 * null ? new Integer(usuario.getCoMuncipioIbge().trim()) : null,
		 * dhUltimoAcesso, this.funcaoList, perfis);
		 */
		// this.user.getUserContextAttributes().put(User.ATTR_IP_LOGIN, this.ip);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean commit() throws LoginException {
		if (!this.succeeded) {
			return false;
		}
		if (this.subject.isReadOnly()) {
			this.cleanState();
			if (this.debug) {
				System.out.println("\t\t[BiosystemLoginModule] Subject is read-only!");
			}
			throw new LoginException("Subject is read-only");
		}
		final Set<Principal> principals = this.subject.getPrincipals();
		if (this.user != null && !principals.contains(user)) {
			principals.add(this.user);
			if (this.debug) {
				System.out.println("\t\t[BiosystemLoginModule] Adicionado User \"" + user + "\"");
			}
		}
		if (this.funcaoList != null && !this.funcaoList.isEmpty()) {
			for (final Funcao funcao : this.funcaoList) {
				if (!principals.contains(funcao)) {
					principals.add(funcao);
					if (this.debug) {
						System.out.println("\t\t[BiosystemLoginModule] Adicionado Funcao \"" + funcao + "\"");
					}
				}
			}
		}
		// JBoss 7 - Inicio
		if (this.isJB7()) {
			final Group roles = new SimpleGroup("Roles");
			if (this.funcaoList != null && !this.funcaoList.isEmpty()) {
				for (final Funcao funcao : this.funcaoList) {
					roles.addMember(funcao);
				}
			}
			final Group subjectGroup = this.createGroup(roles.getName(), principals);
			final Enumeration members = roles.members();
			while (members.hasMoreElements()) {
				subjectGroup.addMember((Principal) members.nextElement());
			}
		}
		// JBoss - Fim
		this.cleanState();
		this.commitSucceeded = true;
		return this.commitSucceeded;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	// JBOSS 7
	protected Group createGroup(final String name, final Set principals) {
		Group roles = null;
		final Iterator iter = principals.iterator();
		while (iter.hasNext()) {
			final Object next = iter.next();
			if ((next instanceof Group) == false) {
				continue;
			}
			final Group grp = (Group) next;
			if (grp.getName().equals(name)) {
				roles = grp;
				break;
			}
		}
		if (roles == null) {
			roles = new SimpleGroup(name);
			principals.add(roles);
		}
		return roles;
	}

	protected boolean isJB7() {
		try {
			Class.forName("org.jboss.security.SimpleGroup");
			return true;
		} catch (final ClassNotFoundException e) {
			return false;
		}
	}

	@Override
	public boolean abort() throws LoginException {
		if (this.debug) {
			System.out.println("\t\t[BiosystemLoginModule] Autenticacao abortada!");
		}
		if (!this.succeeded) {
			return false;
		}
		if (this.succeeded && !this.commitSucceeded) {
			this.succeeded = false;
			this.cleanState();
			this.user = null;
			this.funcaoList = null;
		} else {
			this.logout();
		}
		return true;
	}

	@Override
	public boolean logout() throws LoginException {
		if (this.subject.isReadOnly()) {
			this.cleanState();
			if (debug) {
				System.out.println("\t\t[BiosystemLoginModule] Subject is read-only!");
			}
			throw new LoginException("Subject is read-only");
		}
		final Set<Principal> principals = this.subject.getPrincipals();
		principals.remove(this.user);
		principals.removeAll(this.funcaoList);
		this.cleanState();
		this.succeeded = false;
		this.commitSucceeded = false;
		this.user = null;
		this.funcaoList = null;
		if (debug) {
			System.out.println("\t\t[BiosystemLoginModule] Logout!");
		}
		return true;
	}

	protected void loadUsernamePassword() throws LoginException {
		final Callback[] callbacks = new Callback[2];
		callbacks[0] = new NameCallback("username: ");
		callbacks[1] = new PasswordCallback("password: ", false);
		try {
			this.callbackHandler.handle(callbacks);
			this.username = ((NameCallback) callbacks[0]).getName();

			if (this.username.contains("#")) {
				final String[] temp = this.username.split("#");
				this.username = temp[0];
				this.ip = temp[1];
			} else {
				this.ip = "0.0.0.0";
			}
			final char[] tmpPassword = ((PasswordCallback) callbacks[1]).getPassword();
			this.password = new char[tmpPassword.length];
			System.arraycopy(tmpPassword, 0, this.password, 0, tmpPassword.length);
			((PasswordCallback) callbacks[1]).clearPassword();
		} catch (final Throwable t) {
			if (this.debug) {
				t.printStackTrace();
			}
			throw new LoginException(t.toString());
		}
	}

	protected void cleanState() {
		this.username = null;
		if (this.password != null) {
			Arrays.fill(this.password, ' ');
			this.password = null;
		}
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Funcao> getFuncaoList() {
		return funcaoList;
	}

	public void setFuncaoList(List<Funcao> funcaoList) {
		this.funcaoList = funcaoList;
	}

}
