
if (typeof jQuery === "undefined") {
    throw new Error("AdminLTE requires jQuery");
}

$.bsFixAngular4 = {
    fix: function ajustarLayoutAngular4() {
        var headerHeight = $('.main-header').outerHeight();
        var bsPageHeight = $('.bs-page').outerHeight();
        var window_height = $(window).height();
        var sidebar_height = $(".main-sidebar").height();
        var footerHeight = $('.main-footer').outerHeight();
        if (bsPageHeight < window_height) {
            $(".content-wrapper, .right-side").css('min-height', window_height - (footerHeight + headerHeight));
        } else {
            $(".content-wrapper, .right-side").css('min-height', bsPageHeight);
        }
    }
}