import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApplicationService } from '../../services/application/application.service';
import { LOCAL_STORAGE_CONSTANTS, HEADER_JWT_TOKEN } from '../../constants/constants';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    private appService: ApplicationService,
    private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.appService.getItemFromLocalStorage(LOCAL_STORAGE_CONSTANTS.token);
    if (req.url.indexOf('/service') !== -1) {
      if (token != null && req.url.indexOf('auth/login') == -1 && req.url.indexOf('auth/logout') == -1 && req.url.indexOf('information/instance') == -1) {
        req = req.clone({
          headers: req.headers.set('jwt-token', token)
        });
      }
      req = req.clone({
        headers: req.headers.set('Content-Type', 'application/json')
      });
      req = req.clone({
        headers: req.headers.set('Access-Control-Allow-Origin', '*'),
        withCredentials: true,
        responseType: 'json'
      });

    }

    return next.handle(req).do((ev: HttpEvent<any>) => {
      if (ev instanceof HttpResponse) {
        console.log('processing response', ev);
        const header = ev.headers.get(HEADER_JWT_TOKEN);
        if (header) {
          this.appService.setToken(header);
        }
      }
    }).catch(response => {
      if (response instanceof HttpErrorResponse) {
        console.log('Processing http error', response);
        if (response.status == 0 && response.statusText === 'Unknown Error') {
          const token = this.appService.getToken();
          if (token == null || token === 'undefined') {
            this.router.navigate(['login']);
          } else if (this.appService.isTokenExpired()) {
            this.appService.removeToken();
              this.router.navigate(['login']);
          }
        }
        if (response.status == 401 || response.status == 403) {
          this.appService.resetCurrentView();
          this.appService.clearStorage();
          this.router.navigate(['login']);
        }
      }

      return Observable.throw(response);
    }).finally(() => {
    });
  }
}
