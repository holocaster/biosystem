package br.com.biosystem.loginmodule.entities;

import java.io.Serializable;
import java.security.Principal;
import java.util.List;

public class User implements Principal, Serializable {

	private static final long serialVersionUID = 1L;

	private Long nuCpf;
	private String deCpf;
	private String name;
	private List<Perfil> perfis;

	public User(String deCpf, String name) {
		super();
		this.deCpf = deCpf;
		this.name = name;
	}

	public User(Long nuCpf, String deCpf, String name) {
		super();
		this.nuCpf = nuCpf;
		this.deCpf = deCpf;
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	public Long getNuCpf() {
		return nuCpf;
	}

	public void setNuCpf(Long nuCpf) {
		this.nuCpf = nuCpf;
	}

	public String getDeCpf() {
		return deCpf;
	}

	public void setDeCpf(String deCpf) {
		this.deCpf = deCpf;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nuCpf == null) ? 0 : nuCpf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (nuCpf == null) {
			if (other.nuCpf != null)
				return false;
		} else if (!nuCpf.equals(other.nuCpf))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [nuCpf=" + nuCpf + ", deCpf=" + deCpf + ", name=" + name + ", perfis=" + perfis + "]";
	}
	
	

}
