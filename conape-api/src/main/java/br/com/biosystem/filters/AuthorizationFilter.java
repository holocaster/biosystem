package br.com.biosystem.filters;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import br.com.biosystem.annotations.Credential;
import br.com.biosystem.utils.BiosystemConstants;
import br.com.biosystem.utils.UtilJWT;
import br.com.biosystem.utils.UtilUsuario;

@Provider
public class AuthorizationFilter implements ContainerRequestFilter {

	@Context
	private HttpServletRequest request;

	@Context
	private ResourceInfo resourceInfo;

	@Inject
	private UtilUsuario utilUsuario;

	@Override
	public void filter(final ContainerRequestContext requestContext) throws IOException {

		String token = this.request.getHeader(BiosystemConstants.CABECALHO_JWT_TOKEN);

		if ((token == null) || token.isEmpty()) {
			Method methodInvoked = this.resourceInfo.getResourceMethod();
			Class<?> classInvoked = this.resourceInfo.getResourceClass();

			if (!classInvoked.isAnnotationPresent(PermitAll.class)
					&& !methodInvoked.isAnnotationPresent(PermitAll.class)) {
				if (methodInvoked.isAnnotationPresent(Credential.class)) {
					Credential credential = methodInvoked.getAnnotation(Credential.class);
					String funcionalidade = credential.function();
					String acao = credential.action();

					if (funcionalidade.isEmpty()) {
						if (classInvoked.isAnnotationPresent(Credential.class)) {
							Credential anotacaoCredencialClasse = classInvoked.getAnnotation(Credential.class);
							funcionalidade = anotacaoCredencialClasse.function();
						}
					}

					boolean possuiCredencial = true;
					// this.utilUsuario.contemPermissao(funcionalidade, acao, null, null);

					if (!possuiCredencial) {
						requestContext.abortWith(BiosystemConstants.ACESSO_NAO_PERMITIDO);
					}
				}
			}
		} else {
			if (!UtilJWT.validarToken(token)) {
				HttpSession sessao = this.request.getSession(false);
				if (sessao != null) {
					sessao.removeAttribute("usuarioLogado");
					sessao.invalidate();
					this.utilUsuario.setUsuarioLogado(null);
					this.utilUsuario.setEnderecoIpUsuario(null);
				}
				requestContext.abortWith(BiosystemConstants.TOKEN_INVALIDO_EXPIRADO);
			}
		}
	}

}
