package br.com.biosystem.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "userLogin")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(Include.NON_NULL)
@ApiModel(
        value = "UserLoginRequestDTO",
        description = "Objeto utilizado para representar os atributos da requisicao para fazer login"
)
public class UserLoginRequestDTO {

	@XmlElement(name = "username")
	@ApiModelProperty(value = "Nome do usuario", required = true)
	private String username;

	@XmlElement(name = "password")
	@ApiModelProperty(value = "Senha do usuario", required = true)
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
