import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { ApplicationService , LoginService} from './services/index';

declare var $ : any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit {

  app: any = {};

  constructor(
    private router: Router,
    private appService: ApplicationService) {
    this.app = appService.getApp();
    document.title = this.app.id + ' - ' + this.app.name;
  }
  
  ngOnInit() : void {
  }

  ajustarLayout() {
    if ($.bsFixAngular4) {
      $.bsFixAngular4.fix();
    }
  }

  goLink(link: string) {
    this.router.navigate([link]);
  }

  userLogged(): boolean {
    return !this.appService.isTokenExpired();
  }

  sideBarClass(): string {
    if (!this.appService.isTokenExpired()) {
      return 'sidebar-mini';
    }
    return 'sidebar-collapse';
  }
}
