import { JwtHelper } from 'angular2-jwt';
import { environment } from './../../environments/environment';
import {Component, OnInit} from '@angular/core';
import { HttpClient} from '@angular/common/http'
import {ARQUITETURA_SERVICOS, LOCAL_STORAGE_CONSTANTS} from '../constants/constants'

import {ApplicationService} from '../services/application/application.service';
import {LoginService} from '../services/login.service';
import {AlertMessageService} from '../services/message/alert-message.service';
import { Router } from '@angular/router';
import { User} from '../model/user';
import { LoaderService } from '../services';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent extends AlertMessageService implements OnInit {

  static alreadyLogged = false;

  model: any = {};
  user = {};
  loading = false;

  jwtHelper: JwtHelper = new JwtHelper();

  app : any;

  constructor(private router: Router,
    private loginService: LoginService,
    private appService: ApplicationService,
    private http : HttpClient,
    private loader : LoaderService) {
      super();
      this.app = this.appService.getApp();
  }

  ngOnInit(): void {

  }

  login() {
    this.loading = true;
    this.loader.show();
    this.loginService.login(this.model).subscribe( (res : any) => {
      this.loading = false;
      this.appService.setToken(res.token);
      const user = new User();
      user.cpf = res.cpf;
      user.name = res.name;
      this.appService.saveInLocalStorage(LOCAL_STORAGE_CONSTANTS.user , JSON.stringify(user));
      this.loader.hide();
      this.router.navigate(['principal']);
    }, error => {
      this.loading = false;
      this.loader.hide();
      if (error.status == 401) {
        this.addMessageError('Usuário ou senha inválido');
      } else {
        this.addMessageError('Erro ao comunicar com o servidor');
      }
    });
  }

  logout() {
    this.http.get(environment.serverPath + ARQUITETURA_SERVICOS.logout).subscribe(res => {
      this.appService.clearStorage();

    }, error => {

    });
  }

  isSSO() {
    return environment.ssoLogin;
  }
}
