package br.com.biosystem.rest;

import java.util.HashMap;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(hidden = false, value = "Informacoes sobre o servidor")
@ApiResponses({
    @ApiResponse(code = 500, message = "Falha não mapeada ao processar a requisição.")
})
@Path("/information")
public class InformationRest {
	

    @GET
    @Path("/instance")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    @ApiResponses({
        @ApiResponse(code = 200, message = "Retorna o nome da instancia")
    })
    @ApiOperation(hidden = false, value = "Procura pelo nome do servidor que esta processando a requisicao")
    public Response getNameInstance() {
        HashMap<String, String> retorno = new HashMap<>();
        retorno.put("nomeInstancia", System.getProperty("jboss.node.name"));
        return Response.ok(retorno).build();
    }
}
