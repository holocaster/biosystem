import { ModalMessageComponent } from './../template/modal/modal-message/modal-message.component';
import { DialogService } from "angularx-bootstrap-modal/dist/dialog.service";
import { ModalConfirmComponent } from '../template/modal/modal-confirm/modal-confirm.component';

export class Utils {

    static checkDate(value: any): boolean {
        return !isNaN(Date.parse(value));
    }

    static leftPad(value: any, size: number, pad: string): string {
        let s = value + '';
        while (s.length < size) s = pad + s;
        return s;
    }

    static showMessageDialogMultipleMessages (dialogService : DialogService, messages : string[]) {
        return dialogService
        .addDialog(ModalMessageComponent, {
           title: '',
           msgs: messages
        });
    }

    static showMessageDialog (dialogService : DialogService, message : string) {
        const msg : string[] = [];
        msg.push(message);
        return dialogService
        .addDialog(ModalMessageComponent, {
           title: '',
           msgs: msg
        });
    }

    static showMessageConfirm (dialogService : DialogService, message : string) {
        const msg : string[] = [];
        let resposta:string = "";
        msg.push(message);
        return dialogService
        .addDialog(ModalConfirmComponent, {
           title: '',
           msgs: msg
        });
        
    }
}