package br.com.biosystem.rest;

import java.security.Principal;
import java.text.SimpleDateFormat;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.com.biosystem.dtos.UserLoginRequestDTO;
import br.com.biosystem.dtos.UserLoginResponseDTO;
import br.com.biosystem.loginmodule.entities.User;
import br.com.biosystem.utils.UtilJWT;

@Path("/auth")
public class AuthenticationRest {

	private static final Logger LOG = Logger.getLogger(AuthenticationRest.class);

	protected static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmss");

	@Context
	protected HttpServletRequest request;

	@Resource(lookup = "java:app/AppName")
	protected String appName;

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public Response login(final UserLoginRequestDTO userLoginDTO) {
		Response retorno = null;
		try {
			if (this.request.getUserPrincipal() != null) {
				// Verifica se o usuário logado é o mesmo usuário que está
				// tentando logar
				final User usuario = (User) this.request.getUserPrincipal();
				if (!usuario.getDeCpf().equalsIgnoreCase(userLoginDTO.getUsername())) {
					// Se não for o mesmo usuário, realiza o logout
					this.request.logout();
					// Tenta realizar o login
					this.request.login(userLoginDTO.getUsername(), userLoginDTO.getPassword());
				}
			} else {
				// Tenta realizar o login
				this.request.login(userLoginDTO.getUsername(), userLoginDTO.getPassword());
			}

			// Obtem o usuario logado
			final Principal usuarioLogado = this.request.getUserPrincipal();

			// Caso exista o usuário logado
			if (usuarioLogado != null) {
				final User user = (User) usuarioLogado;

				retorno = convertToUserDTO(user);
			} else {
				retorno = Response.status(403).build();
			}
		} catch (final ServletException e) {
			retorno = Response.status(401).build();
			AuthenticationRest.LOG.error("Erro ao realizar login", e);

		}

		return retorno;
	}

	@GET
	@Path("/logout")
	public Response logout() {
		Response retorno = null;
		try {
			this.request.logout();

			HttpSession sessao = this.request.getSession(false);

			if (sessao != null) {
				sessao.invalidate();
			}

			retorno = Response.ok("").build();
		} catch (final ServletException e) {
			retorno = Response.status(401).build();
			AuthenticationRest.LOG.error("Erro ao realizar logout", e);
		}

		return retorno;
	}

	private UserLoginResponseDTO loadUserDTO(final User usuario) {
		final UserLoginResponseDTO userLoginResponseDTO = new UserLoginResponseDTO();

		userLoginResponseDTO.setCpf(usuario.getDeCpf());
		userLoginResponseDTO.setName(usuario.getName());

		// Identifica o perfil do usuario
		/*
		 * String perfil = ""; for (final CaixaPerfil caixaPerfil : usuario.getPerfis())
		 * { perfil = caixaPerfil.getNoPerfil(); }
		 * 
		 * userLoginResponseDTO.setPerfil(perfil);
		 * userLoginResponseDTO.setCredenciais(usuario.getRoles());
		 */

		return userLoginResponseDTO;
	}

	private Response convertToUserDTO(final User user) {

		final UserLoginResponseDTO result = this.loadUserDTO(user);
		result.setToken(UtilJWT.gerarToken(user));

		return Response.ok(result).build();
	}

}
