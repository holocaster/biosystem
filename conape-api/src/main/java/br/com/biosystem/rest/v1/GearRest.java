package br.com.biosystem.rest.v1;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.logging.Log;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(hidden = false, value = "CRUD para peças")
@ApiResponses({
    @ApiResponse(code = 500, message = "Falha não mapeada ao processar a requisição.")
})
@Path("/v1/gear")
public class GearRest {

	@Inject
	private Log log;
	
	@GET
	public Response listAll(@Context UriInfo uriInfo) {
		log.debug("Carregando todas as peças");
		
		return Response.ok(new ArrayList<>()).build();
	}
	
	@GET
	@Path("error")
	public Response listError(@QueryParam("id") int id) {
		log.debug("Mock para testar error");
		
		return Response.status(id).build();
	}
}
