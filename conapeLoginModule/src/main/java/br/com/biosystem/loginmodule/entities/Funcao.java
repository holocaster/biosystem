package br.com.biosystem.loginmodule.entities;

import java.io.Serializable;
import java.security.Principal;

public class Funcao implements Principal, Serializable {

	private static final long serialVersionUID = 1L;
	private String funcionalidade;
	private String acao;

	public Funcao() {

	}

	public Funcao(String funcionalidade, String acao) {
		super();
		this.funcionalidade = funcionalidade;
		this.acao = acao;
	}

	public String getFuncionalidade() {
		return funcionalidade;
	}

	public void setFuncionalidade(String funcionalidade) {
		this.funcionalidade = funcionalidade;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acao == null) ? 0 : acao.hashCode());
		result = prime * result + ((funcionalidade == null) ? 0 : funcionalidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Funcao))
			return false;
		Funcao other = (Funcao) obj;
		if (acao == null) {
			if (other.acao != null)
				return false;
		} else if (!acao.equals(other.acao))
			return false;
		if (funcionalidade == null) {
			if (other.funcionalidade != null)
				return false;
		} else if (!funcionalidade.equals(other.funcionalidade))
			return false;
		return true;
	}

	@Override
	public String getName() {
		return this.funcionalidade + (this.acao != null ? "_" + this.acao : "");
	}

	@Override
	public String toString() {
		return "Funcao [funcionalidade=" + funcionalidade + ", acao=" + acao + "]";
	}

}
