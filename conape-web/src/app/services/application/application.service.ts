import { environment } from './../../../environments/environment.prod';
import {Injectable} from '@angular/core';
import * as data from './../../../assets/project.json';
import {LOCAL_STORAGE_CONSTANTS} from './../../constants/constants';
import {JwtHelper} from 'angular2-jwt';

@Injectable()
export class ApplicationService {

  private app: any = {};

  private appName;

  private currentiView: any = {};

  private jwtHelper : JwtHelper;

  constructor() {
    this.app = data;
    this.appName = this.app['id'];
    this.jwtHelper = new JwtHelper();
  }

  hasCredential(funcionalidade : string , acao : string) : boolean {
    const user = JSON.parse(this.getItemFromLocalStorage(LOCAL_STORAGE_CONSTANTS.token));
    if (user != null) {
      for (const item of user.credenciais) {
        if (funcionalidade === item.noFuncionalidade) {
          if (acao) {
            if (acao === item.noAcao) {
              return true;
            } else {
              continue;
            }
          } else {
            return true;
          }
        } 
      }
    }

    return false;
  }

  saveInLocalStorage(id: string, value: any) {
    localStorage.setItem(this.appName + '-' + id, value);
  }

  removeInLocalStorage(id: string) {
    localStorage.removeItem(this.appName + '-' + id);
  }

  getItemFromLocalStorage(id: string) {
    return localStorage.getItem(this.appName + '-' + id);
  }

  clearStorage(): void {
    localStorage.clear();
  }

  resetCurrentView() {
    this.currentiView = {};
  }

  setCurrentView(value: string) {
    for (const view of this.app['views']) {
      if (view.id === value) {
        this.currentiView = view;
        break;
      }
    }
  }

  isTokenExpired() {
    const token = this.getToken();
    return token == null || token == undefined || this.jwtHelper.isTokenExpired(token);
  }

  getToken () : string {
    return localStorage.getItem(this.appName + '-' + LOCAL_STORAGE_CONSTANTS.token);
  }

  setToken(token) {
    localStorage.setItem(this.appName + '-' + LOCAL_STORAGE_CONSTANTS.token, token);
  }

  removeToken() {
    localStorage.removeItem(this.appName + '-' + LOCAL_STORAGE_CONSTANTS.token);
  }

  getCurrentView() {
    return this.currentiView;
  }

  getWidgetsProperties(id: string) {
    return this.app['widgets'][id];
  }

  getProperty(id: string) {
    return this.app[id];
  }

  getApp() {
    return this.app;
  }

}
