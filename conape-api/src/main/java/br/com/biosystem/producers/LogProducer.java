package br.com.biosystem.producers;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LogProducer {

	@Produces
	public Log produce(InjectionPoint ip) {
		return LogFactory.getLog(ip.getMember().getDeclaringClass());
	}
}
