import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelaInicialComponent } from './tela-inicial.component';
import { TemplateModule } from '../template/template.module';
import { HeaderComponent } from '../template';

@NgModule({
  imports: [
    CommonModule,
    TemplateModule
  ],
  declarations: [
    TelaInicialComponent
  ],
  exports : [
    TelaInicialComponent
  ]
})
export class TelaInicialModule { }
