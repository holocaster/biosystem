package br.com.biosystem.exceptions;

public class ExpiredTokenException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ExpiredTokenException() {
	}

	public ExpiredTokenException(String message) {
		super(message);
	}

}
