import { Component, OnInit, AfterViewInit , AfterViewChecked} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationService, EventService , LoginService} from './../../services/index';
import { EVENTS, LOCAL_STORAGE_CONSTANTS } from './../../constants/constants';
import { environment } from './../../../environments/environment';
import { User} from '../../model/user';

declare var $: any;

const TELA_LOGIN = 'login';

@Component({
  selector: 'bs-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit , AfterViewChecked {
  app: any = {};
  user : User;
  nomeUnidade: string;
  filtrosvalor : any = {};
  notificacoes = [];
  currentiView;
  userImage: any;
  userImageDefault: any;
  hasImage = false;
  urlManualUsuario: string;
  currentView;

  constructor(private appService: ApplicationService, private loginService: LoginService, private http: HttpClient, private eventService: EventService) {}

  ngOnInit() {
    this.app = this.appService.getApp();
    this.currentView = this.appService.getCurrentView();
    this.filtrosvalor['filtrosvalor'] = '';
  }

  ngAfterViewInit(): void {
    this.eventService.on(EVENTS.authorization, auth => {
      this.init();
    });
  }

  ngAfterViewChecked() {
    if (!this.appService.isTokenExpired()) {
      this.getUserImage();
    }

    if (this.user == null) {
      this.user = JSON.parse(this.appService.getItemFromLocalStorage(LOCAL_STORAGE_CONSTANTS.user));
    }
      
  }

  private init() {
    this.user = JSON.parse(this.appService.getItemFromLocalStorage(LOCAL_STORAGE_CONSTANTS.user));
  }


  isApresentaBarraPesquisa() {
    return this.appService.getCurrentView()['id'] != undefined && this.appService.getCurrentView()['id'] != TELA_LOGIN && !this.appService.getCurrentView()['hideSearchBar'];
  }

  isApresentaPerfilUsuario() {
    return this.appService.getCurrentView()['id'] != undefined && this.appService.getCurrentView()['id'] != TELA_LOGIN && !this.appService.getCurrentView()['hideUserProfile'] && this.hasUser();
  }

  private hasUser() {
    return this.user != null && this.user != undefined;
  }

  isApresentaNotificacoes() {
    return this.appService.getCurrentView()['id'] != undefined && this.appService.getCurrentView()['id'] != TELA_LOGIN && !this.appService.getCurrentView()['hideNotifications'];
  }

  isApresentaWidgetSidebar() {
    return this.appService.getCurrentView()['id'] != undefined && this.appService.getCurrentView()['id'] != TELA_LOGIN && !this.appService.getCurrentView()['hideWidgetSidebar'];
  }

  ativarToogle() {
    if ($.AdminLTE.controlSidebar) {
      $('[data-toggle="control-sidebar"]').unbind('click');
      $.AdminLTE.controlSidebar.activate();
    }
  }

  getCpf() {
    return this.user.cpf;
  }

  getName() {
    return this.user.name;
  }

  getPerfil() {
    if (this.user.perfil) {
      return this.user.perfil;
    }
    return 'Sem Perfil Definido';
  }

  clean() {
    this.filtrosvalor = {};
    this.eventService.broadcast(EVENTS.cleanHeaderSearch);
  }

  search() {
    this.eventService.broadcast(EVENTS.headerSearch, this.filtrosvalor);
  }

  getBufferUserImage() {
    return 'data:image/jpg;base64,' + this.userImage;
  }

  private getUserImage() {
    this.userImageDefault = this.appService.getProperty('userImagePadrao');
    this.hasImage = false;
  }

  private converterArrayBufferToBase64(buffer) {
    let binary = '';
    const bytes = new Uint8Array(buffer);
    const len = bytes.byteLength;

    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }

    return window.btoa(binary);
  }

  logout() {
    this.loginService.logout();
  }

  isAutenticado() {
    return !this.appService.isTokenExpired();
  }
}
