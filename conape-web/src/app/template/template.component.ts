import { Component, OnInit } from '@angular/core';

import { MenuComponent } from './menu/menu.component';

@Component( {
    selector: 'bs-template',
    templateUrl: './template.component.html',
    styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
