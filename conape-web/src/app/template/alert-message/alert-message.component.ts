import { EventService } from './../../services/event-service/event-service';
import { EVENTS, ALERT_MESSAGE_SUCCESS, ALERT_MESSAGE_INFO, ALERT_MESSAGE_ERROR, ALERT_MESSAGE_WARNING } from './../../constants/constants';
import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.css']
})
export class AlertMessageComponent implements OnInit, OnChanges {
  @ViewChild('alertMessages') alertMessages: ElementRef;

  @Input() messagesSuccess: string[];
  @Input() messagesInfo: string[];
  @Input() messagesError: string[];
  @Input() messagesWarning: string[];

  @Output() onCloseMessageSuccess = new EventEmitter<any>();
  @Output() onCloseMessageInfo = new EventEmitter<any>();
  @Output() onCloseMessageError = new EventEmitter<any>();
  @Output() onCloseMessageWarning = new EventEmitter<any>();


  constructor(private eventService: EventService) {
    this.messagesSuccess = [];
    this.messagesInfo = [];
    this.messagesError = [];
    this.messagesWarning = [];
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      this.alertMessages.nativeElement.focus();
    }
  }
}
