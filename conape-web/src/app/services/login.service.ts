import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import {ARQUITETURA_SERVICOS} from '../constants/constants';
import { environment } from '../../environments/environment';
import { ApplicationService } from './application/application.service';
import { Router } from '@angular/router';

@Injectable()
export class LoginService {

  constructor(private http : HttpClient,
    private router : Router,
    private appService : ApplicationService)
   { }

  login(loginJson) {
    return this.http.post(environment.serverPath + ARQUITETURA_SERVICOS.login, JSON.stringify(loginJson) );
  }

  logout() {
    this.http.get(environment.serverPath + ARQUITETURA_SERVICOS.logout).subscribe(res => {
      console.log('voltou certo do logout');
      this.resetAndGoLogin();
    }, error => {
      console.log('voltou errado do logout');
      this.resetAndGoLogin();
    });
  }

  private resetAndGoLogin() : void {
    this.appService.clearStorage();
    this.appService.resetCurrentView();
    this.router.navigate(['login']);
  }

  usuarioAutenticado() {
    return this.appService.isTokenExpired();
 }

}
