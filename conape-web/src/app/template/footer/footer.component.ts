import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApplicationService} from '../../services/index';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'bs-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit{


  app: any = {}
  instancia: string;

  constructor(private appService: ApplicationService, 
    private http: HttpClient) {}

  ngOnInit() {
    this.app = this.appService.getApp();
    this.getInstancia();
    
  }
  
  private getInstancia() {
    this.http.get(environment.serverPath + '/information/instance').subscribe(data => {
      this.instancia = data['nomeInstancia'];
    });
  }

}
