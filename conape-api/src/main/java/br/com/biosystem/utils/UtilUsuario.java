
package br.com.biosystem.utils;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.com.biosystem.loginmodule.entities.User;

@RequestScoped
public class UtilUsuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String X_REAL_IP = "X-Real-IP";

	private static final String X_FORWARDED_FOR = "X-Forwarded-For";

	private User usuarioLogado;

	private String enderecoIpUsuario;

	private String token;

	@Inject
	private HttpServletRequest request;

	public UtilUsuario() {
	}

	public void setUsuarioLogado(User usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public User getUsuarioLogado() {
		if (usuarioLogado == null) {
			token = this.request.getHeader(BiosystemConstants.CABECALHO_JWT_TOKEN);
			if (token != null) {
				usuarioLogado = UtilJWT.obterUserDoToken(token);
			}
		}
		return usuarioLogado;
	}

	/**
	 * Retorna o valor do atributo enderecoIpUsuario.
	 *
	 * @return enderecoIpUsuario
	 */
	public String getEnderecoIpUsuario() {
		if (this.enderecoIpUsuario == null || this.enderecoIpUsuario.trim().isEmpty()) {
			this.enderecoIpUsuario = this.getIpUsuario();
		}
		return enderecoIpUsuario;
	}

	/**
	 * Define o valor do atributo enderecoIpUsuario.
	 *
	 * @param enderecoIpUsuario
	 *            valor a ser atribuído
	 */
	public void setEnderecoIpUsuario(String enderecoIpUsuario) {
		this.enderecoIpUsuario = enderecoIpUsuario;
	}

	/**
	 * Retorna o valor do endereço IP do usuário passado pelo Nginx
	 * 
	 * @return {@link String} IP do usuário
	 */
	public String getIpUsuario() {

		if (this.request.getHeader(X_REAL_IP) != null) {
			return this.request.getHeader(X_REAL_IP);
		}

		if (this.request.getHeader(X_FORWARDED_FOR) != null) {
			return this.request.getHeader(X_FORWARDED_FOR);
		}

		return this.request.getRemoteAddr();
	}

	/**
	 * 
	 * <p>
	 * Método responsável por recuperar o DNS da máquina do usuário
	 * </p>
	 * .
	 *
	 * @return
	 *
	 * @author TO-Brasil
	 */
	public String getDnsUsuario() {
		if (this.enderecoIpUsuario == null) {
			this.enderecoIpUsuario = this.getIpUsuario();
		}
		try {
			return InetAddress.getByName(this.enderecoIpUsuario).getHostName();
		} catch (UnknownHostException e) {
		}

		return null;
	}
}
