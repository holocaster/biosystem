import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {
  KzCepPipe,
  KzCpfPipe,
  KzCnpjPipe,
  KzCpfCnpjPipe,
  KzCpfValidatorDirective,
  KzCnpjValidatorDirective,
  KzCpfCnpjValidatorDirective,
  KzTelefoneValidatorDirective,
  ModalUtilComponent,
  KzPaginacaoComponent,
  KzMaskDirective,
  KzMaskCurrencyDirective,
  KzPikadayDirective
} from './';
import { BsOnlyNumberDirective } from './directives/bs-only-number.directive';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [
    KzCepPipe,
    KzCpfPipe,
    KzCnpjPipe,
    KzCpfCnpjPipe,
    KzCpfValidatorDirective,
    KzCnpjValidatorDirective,
    KzCpfCnpjValidatorDirective,
    KzTelefoneValidatorDirective,
    ModalUtilComponent,
    KzPaginacaoComponent,
    KzMaskDirective,
    KzMaskCurrencyDirective,
    KzPikadayDirective,
    BsOnlyNumberDirective
  ],
  exports: [
    KzCepPipe,
    KzCpfPipe,
    KzCnpjPipe,
    KzCpfCnpjPipe,
    KzCpfValidatorDirective,
    KzCnpjValidatorDirective,
    KzCpfCnpjValidatorDirective,
    KzTelefoneValidatorDirective,
    ModalUtilComponent,
    KzPaginacaoComponent,
    KzMaskDirective,
    KzMaskCurrencyDirective,
    KzPikadayDirective,
    CommonModule,
    FormsModule,
    BsOnlyNumberDirective
  ]
})
export class MaskModule {}
