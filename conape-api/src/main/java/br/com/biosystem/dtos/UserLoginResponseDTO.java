package br.com.biosystem.dtos;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.biosystem.loginmodule.entities.Funcao;

@XmlRootElement(name = "userResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserLoginResponseDTO {

	@XmlElement(name = "cpf")
	private String cpf;

	@XmlElement(name = "name")
	private String name;

	@XmlElement(name = "token")
	private String token;

	@XmlElementWrapper(name = "funcoes")
	@XmlElement(name = "funcoes")
	@JsonProperty(value = "funcoes")
	private List<Funcao> funcoes;

	public void addFuncao(final Funcao funcao) {
		if (this.funcoes == null) {
			this.funcoes = new ArrayList<>();
		}

		this.funcoes.add(funcao);
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<Funcao> getFuncoes() {
		return funcoes;
	}

}
