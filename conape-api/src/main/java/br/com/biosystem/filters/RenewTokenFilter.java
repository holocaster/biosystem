package br.com.biosystem.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import br.com.biosystem.loginmodule.entities.User;
import br.com.biosystem.utils.BiosystemConstants;
import br.com.biosystem.utils.UtilJWT;
import br.com.biosystem.utils.UtilUsuario;

@Provider
public class RenewTokenFilter implements ContainerResponseFilter {

	@Context
	private HttpServletRequest request;

	@Inject
	private UtilUsuario utilUsuario;

	@Override
	public void filter(final ContainerRequestContext requestContext, final ContainerResponseContext responseContext)
			throws IOException {

		User usuario = null;

		try {
			usuario = utilUsuario.getUsuarioLogado();

			if (usuario != null) {
				// Gera o token JWT para enviar na resposta
				final String novoToken = UtilJWT.gerarToken(utilUsuario.getUsuarioLogado());

				responseContext.getHeaders().putSingle(BiosystemConstants.CABECALHO_JWT_TOKEN, novoToken);
			}
		} catch (Exception e) {
			// Token Expirado
		}

	}
}
