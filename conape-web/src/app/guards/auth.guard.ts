import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {ApplicationService, EventService, LoginService} from '../services/index';
import {EVENTS} from '../constants/constants';


@Injectable()
export class AuthGuard implements CanActivate , CanActivateChild {
 

  constructor(private router: Router,
    private appService: ApplicationService,
    private eventService : EventService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.appService.isTokenExpired()) {
      if (state.url === '/' || state.url.indexOf('login') !== -1) {
        this.eventService.broadcast(EVENTS.authorization , true)
        this.router.navigate(['principal']);
        return true;
      }
      this.appService.setCurrentView(state.url.substring(1));
      return true;
    }
    this.appService.clearStorage();
    this.appService.setCurrentView('login');
    this.router.navigate(['login'])
    return false;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.appService.setCurrentView(state.url.substring(1));
    return true;
  }
}