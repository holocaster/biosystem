export const environment = {
  production: false,
  ssoLogin: false,
  serverPath: 'http://localhost:8080/conape-api/service',
  showSSOInfo : true
};