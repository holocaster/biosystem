import { AlertMessageComponent } from './alert-message/alert-message.component';
import { KeycloakModule, Keycloak, KeycloakAuthorization, KeycloakHttp } from '@ebondu/angular2-keycloak';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../login/index';
import { BackComponent } from '../back/index';
import { TemplateComponent, MenuComponent, HeaderComponent, FooterComponent, MenuFilterPipe, HeaderWidgetComponent } from './index';
import { LoaderComponent } from '../services/index';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    KeycloakModule.forRoot(),
    FormsModule,
    RouterModule
  ],
  declarations: [
    LoginComponent,
    BackComponent,
    TemplateComponent,
    MenuComponent,
    HeaderComponent,
    FooterComponent,
    MenuFilterPipe,
    HeaderWidgetComponent,
    LoaderComponent,
    AlertMessageComponent
  ],
  providers : [
    Keycloak,
    KeycloakAuthorization,
    KeycloakHttp,
  ]
  ,
  exports : [
    LoginComponent,
    BackComponent,
    TemplateComponent,
    MenuComponent,
    HeaderComponent,
    FooterComponent,
    MenuFilterPipe,
    HeaderWidgetComponent,
    LoaderComponent,
    AlertMessageComponent
  ]
})
export class TemplateModule { }
