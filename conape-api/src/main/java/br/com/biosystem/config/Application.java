package br.com.biosystem.config;


import javax.ws.rs.ApplicationPath;

import io.swagger.annotations.Api;

@Api
@ApplicationPath("service")
public class Application extends javax.ws.rs.core.Application {

}
