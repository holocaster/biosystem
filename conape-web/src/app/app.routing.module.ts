import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/index';
import { LoginComponent } from './login/index';
import { TemplateComponent } from './template/index';
import { TelaInicialComponent } from './tela-inicial/tela-inicial.component';

const routes: Routes = [
  {
    path: '', component: TemplateComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard], children:
    [
      { path: 'principal', component: TelaInicialComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }