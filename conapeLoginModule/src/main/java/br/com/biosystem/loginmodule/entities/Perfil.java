package br.com.biosystem.loginmodule.entities;

import java.io.Serializable;

public class Perfil implements Serializable {

	private static final long serialVersionUID = 1L;
	private String noPerfil;
	private String dePerfil;

	public Perfil() {

	}

	public Perfil(String noPerfil, String dePerfil) {
		super();
		this.noPerfil = noPerfil;
		this.dePerfil = dePerfil;
	}

	public String getNoPerfil() {
		return noPerfil;
	}

	public void setNoPerfil(String noPerfil) {
		this.noPerfil = noPerfil;
	}

	public String getDePerfil() {
		return dePerfil;
	}

	public void setDePerfil(String dePerfil) {
		this.dePerfil = dePerfil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noPerfil == null) ? 0 : noPerfil.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Perfil))
			return false;
		Perfil other = (Perfil) obj;
		if (noPerfil == null) {
			if (other.noPerfil != null)
				return false;
		} else if (!noPerfil.equals(other.noPerfil))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Perfil [noPerfil=" + noPerfil + ", dePerfil=" + dePerfil + "]";
	}

}
