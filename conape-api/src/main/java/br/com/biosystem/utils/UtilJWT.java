package br.com.biosystem.utils;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import br.com.biosystem.exceptions.ExpiredTokenException;
import br.com.biosystem.loginmodule.entities.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class UtilJWT {

	private static final Logger LOG = Logger.getLogger(UtilJWT.class);

	private static String CHAVE_JWT = "GITEC-GO";

	private static String LIMITE_EXPIRACAO_TOKEN = "30";

	private static String APP_NAME = "";

	public UtilJWT() throws NamingException {
		String appName = (String) new javax.naming.InitialContext().lookup("java:app/AppName");
		APP_NAME = appName.substring(0, 5).toLowerCase();
	}

	public static String gerarToken(final User user) {
		String token = null;

		try {

			UtilJWT.obterValoresPropriedade();

			// Data atual
			final long nowMillis = System.currentTimeMillis();
			final Date now = new Date(nowMillis);

			final long limiteLong = Long.valueOf(UtilJWT.LIMITE_EXPIRACAO_TOKEN) * 60 * 1000;

			// Define a data de expiração do token
			final Date expirationDate = new Date(nowMillis + limiteLong);

			// Monta o token que será enviado na resposta
			token = Jwts.builder().setIssuedAt(now).claim("deCpf", user.getDeCpf()).claim("name", user.getName())
					.signWith(SignatureAlgorithm.HS256, UtilJWT.CHAVE_JWT).setExpiration(expirationDate).compact();

		} catch (final Exception e) {
			UtilJWT.LOG.error("Ocorreu erro ao gerar o token JWT.", e);
		}

		return token;
	}

	public static Boolean validarToken(final String token) {
		Boolean tokenValido = false;

		// Data atual
		final long nowMillis = System.currentTimeMillis();
		final Date now = new Date(nowMillis);

		UtilJWT.obterValoresPropriedade();

		try {
			// Valida o token
			final Claims claims = Jwts.parser().setSigningKey(UtilJWT.CHAVE_JWT).parseClaimsJws(token).getBody();

			// Verifica se o token expirou
			if (claims.getExpiration().compareTo(now) > 0) {
				tokenValido = true;
			} else {
				UtilJWT.LOG.info("Token expirado:" + token);
			}
		} catch (final Exception e) {
			UtilJWT.LOG.info("Token inválido: " + token, e);
		}

		return tokenValido;
	}

	private static void obterValoresPropriedade() {

		try {
			String appName = (String) new javax.naming.InitialContext().lookup("java:app/AppName");
			APP_NAME = appName.substring(0, 5).toLowerCase();
		} catch (NamingException e) {
			e.printStackTrace();
		}

		if (APP_NAME != null) {
			String chave = System.getProperty(APP_NAME.concat(".chave.jwt"));

			if (chave != null) {
				UtilJWT.CHAVE_JWT = chave;
			}

			String limite = System.getProperty(APP_NAME.concat(".limite.expiracao.token"));

			if (limite != null) {
				UtilJWT.LIMITE_EXPIRACAO_TOKEN = limite;
			}
		}
	}

	public static User obterUserDoToken(final String token) {
		User usuario = null;

		UtilJWT.obterValoresPropriedade();

		if (token != null) {
			try {
				// Valida o token
				final Claims claims = Jwts.parser().setSigningKey(UtilJWT.CHAVE_JWT).parseClaimsJws(token).getBody();

				usuario = new User(claims.get("deCpf", String.class), claims.get("name", String.class));
			} catch (final ExpiredJwtException e) {
				throw new ExpiredTokenException("Token expirado: " + token);
			} catch (final Exception e) {
				UtilJWT.LOG.info("Token inválido: " + token, e);
			}
		}
		return usuario;
	}

	public static List<String> getHeaderAndPayloadDecoded(final String token) {
		if (token == null || token.trim().isEmpty()) {
			throw new IllegalArgumentException("Token nulo");
		}

		final String[] split = token.split("\\.");

		if (split.length != 3) {
			throw new IllegalArgumentException("Token não contem as partes necessárias");
		}

		final List<String> result = new ArrayList<>();

		for (int i = 0; i < split.length; i++) {
			if (i < split.length - 1) {
				result.add(new String(Base64.getDecoder().decode(split[i].getBytes())));
			}
		}

		return result;
	}

	public static String getPayloadDecoded(final String token) {
		return UtilJWT.getHeaderAndPayloadDecoded(token).get(1);
	}

	public static String getHeaderDecoded(final String token) {
		return UtilJWT.getHeaderAndPayloadDecoded(token).get(0);
	}

}
