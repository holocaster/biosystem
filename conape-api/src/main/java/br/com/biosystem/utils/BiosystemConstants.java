package br.com.biosystem.utils;

import javax.ws.rs.core.Response;

public final class BiosystemConstants {

	private BiosystemConstants() {
	}

	public static final String CABECALHO_JWT_TOKEN = "jwt-token";

	public static final Response ACESSO_NAO_AUTORIZADO = Response.status(Response.Status.UNAUTHORIZED).build();

	public static final Response ACESSO_NAO_PERMITIDO = Response.status(Response.Status.FORBIDDEN).build();

	public static final Response TOKEN_INVALIDO_EXPIRADO = Response.status(440).build();

}
