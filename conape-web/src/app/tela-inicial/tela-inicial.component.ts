import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ARQUITETURA_SERVICOS } from '../constants/constants';

@Component({
  selector: 'tela-inicial',
  templateUrl: './tela-inicial.component.html',
  styleUrls: ['./tela-inicial.component.css']
})
export class TelaInicialComponent implements OnInit {

  constructor(private http : HttpClient) { }

  ngOnInit() {
  }

  call() {
    this.http.get(environment.serverPath + ARQUITETURA_SERVICOS.gear).subscribe(res => {
      console.log(res);
    } , error => {
      console.error(error);
    })
  }

  callError(id) {
    this.http.get(environment.serverPath + ARQUITETURA_SERVICOS.gear + '/error?id=' + id).subscribe(res => {
      console.log(res);
    } , error => {
      console.error(error);
    })
  }

}
