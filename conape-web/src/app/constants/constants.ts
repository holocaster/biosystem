export const ARQUITETURA_SERVICOS = {
  login: '/auth/login',
  logout: '/auth/logout',
  gear: '/v1/gear'
}

export const LOCAL_STORAGE_CONSTANTS = {
  token: 'token',
  user: 'user'
}


export const EVENTS = {
  authorization: 'authorization',
  cleanHeaderSearch: 'cleanHeaderSearch',
  headerSearch: 'headerSearch',
  alertMessage: 'alertMessage',
  finishSaveDossieCliente : 'finishSaveDossieCliente'
}

export const ALERT_MESSAGE_SUCCESS = 1;
export const ALERT_MESSAGE_INFO = 2;
export const ALERT_MESSAGE_ERROR = 3;
export const ALERT_MESSAGE_WARNING = 4;

export const HEADER_JWT_TOKEN = 'jwt-token';